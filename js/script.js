/**
 * Created by Andrei on 12.07.2016.
 */
$(document).ready(function() {
    $(window).scroll(function(){
        if ( $(window).scrollTop() >= 500 ){
            $('header').css({'background-color':'#e4e4e4', 'transition' : 'background-color 0.3s'});
        }
        if( $(window).scrollTop() < 500 ){
            $('header').css({'background-color':'transparent', 'transition' : 'background-color 0.5s'});
        }
    });
});